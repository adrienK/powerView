// requete simple on donne uri ca print les infos,
// -o redirection vers un output personalisé
// -w retourne des infos selement en cas de warning
// -t la location du bat file
// si en charge afficher temps avant charge complette et % de charge actuel
// 0% charged, ~11h to complette
// 0% Battrie, ~11h left

use std::str;
use std::f32;
use std::env;
use std::fs::File;
use std::io::{Read, Write, stdout};
use std::path::Path;
use std::time::Duration;
use std::thread::sleep;
use std::process::{exit, Command};

struct Args {
    target: String,
    out   : String,
    warn  : u32
}

struct PowerLevel {
    stat   : String,
    current: u32,
    charge : u32,
    full   : u32,
    percent: u32,
    time   : f32
}

trait Round_ {
    fn round_(self, n: i32) -> Self;
}

impl Round_  for f32 {
    fn round_(self, n: i32) -> Self {
        let preci = 10.0f32.powi(n);

        if self < 0.0 {
            (self / preci).round() * preci
        } else if self > 0.0 {
            (self * preci).round() / preci
        } else {
            self
        }
    }
}

fn main() {

    let args : Args = get_args();

    let mut warn : u32;
    let mut data : String;
    let mut power: PowerLevel;
    
    warn = args.warn;

    loop {
        data  = get_data(&args.target);
        power = parse_data(data);
        
        if 0 == args.warn {
            print_power(power, &args.out);
            break;
        }

        if "Discharging" != power.stat {
            if args.warn != warn {
                warn = args.warn;
            }
        } else if power.percent <= warn {
            warn = warn / 2;
            print_power(power, &args.out);
        }

        sleep(Duration::from_secs(30));
    }
}

fn help() -> () {
    stdout().write(b"
-- Help for powerState V.1 --

You can use -w and -o to gether

-t <uevent>     Uevent BAT fille location
-o <arg>        None standard outPut
-w <perc>       Loop put warning wen BAT life <perc>

Ex:
//Give pStatus
./powerState -t <loc>/uevent

//Give pStatus on ratboison status bar
./powerState -t <loc>/uevent -o \"ratpoison -c\" 'echo {}'

//Give pStatus only if power percent left is < 10%
./powerState -t <loc>/uevent -w 10
    ");

    exit(-1);
}

fn get_args() -> Args {
    let mut args: Args  = Args {
        target: "".to_string(),
        out   : "".to_string(),
        warn  : 0
    };

    let argv: Vec<_> = env::args().collect();
    let argc: usize  = argv.len();

    if argc < 3 {
        help();
    }

    for i in 0..argc {
        if !argv[i].starts_with("-") {
            continue;
        }

        match argv[i].as_ref() {
            "-o" => {
                if (argc - 1) >= (i + 1) {
                    args.out = argv[i + 1].to_string();
                }
            },
            "-t" => {
                if (argc - 1) >= (i + 1) {
                    args.target = argv[i + 1].to_string();
                }
            },
             "-w" => {
                if (argc - 1) >= (i + 1) {
                    args.warn = argv[i + 1].parse().unwrap();
                }
            },
            _ => help()
        }
    }

    args
}

fn get_data(trg: &str) -> String {
    let mut data = Vec::new();
    let mut file = File::open(Path::new(trg)).expect("Unable to open file");
    
    file.read_to_end(&mut data).expect("Unable to read data");
    let s = match str::from_utf8(&data) {
        Ok(v)  => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    };

    s.to_string()
}

fn parse_data(data: String) -> PowerLevel {
    let mut power = PowerLevel {
        stat   : "".to_string(),
        current: 0,
        charge : 0,
        full   : 0,
        percent: 0,
        time   : 0.
    };

    for i in data.lines() {
        if i.contains("STATUS") {
            power.stat    = i.split('=').nth(1).unwrap().to_string();
        } else if i.contains("CURRENT_NOW") {
            power.current = int_value(i);
        } else if i.contains("CHARGE_NOW") {
            power.charge  = int_value(i);
        } else if i.contains("CHARGE_FULL=") {
            power.full    = int_value(i);
        }
    }

    power.percent = power.charge * 100 / power.full;
    power.time    = ((power.charge) as f32 / (power.current) as f32).round_(2);

    power
}

fn print_power(power: PowerLevel, format: &str) -> ()
{
// 0% Battrie, ~11h left
    let mut out: String = "".to_string();

    if "Discharging" == power.stat {
        /*out.push_str(&power.percent.to_string());
        out.push_str("% Batterie, ~");
        out.push_str(&power.time.to_string());
        out.push_str("h left");*/
        out = power.percent.to_string()
            + "% Batterie, ~"
            + &power.time.to_string()
            + "h left";
    } else {
        out = power.percent.to_string()
            + "% Charged";
    }

    if "" != format {
        out = format.replace("{}", &out);
        Command::new("sh").arg("-c").arg(out).output();
        //println!("{}", out);
    } else {
        println!("{}", out);
    }
}

fn int_value(value: &str) -> u32 {
    let get: u32 = value.split('=').nth(1).unwrap().parse().unwrap();

    get
}
